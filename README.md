##EMSICA
There is trial implementation of the EMSICA algorithm. 
To start, set all the folders in the MATLAB path and run the command 'emsica'.

**Input data:** leadfield matrix fail (*.mat), EEG data (*.set). Leadfield matrix is got using [BrainSuite](http://brainsuite.org/) and [Brainstorm](https://neuroimage.usc.edu/brainstorm/Introduction).

**Output data:** EEG data with new components (result.set).

To visually see the result, you need to open the received file in the [EEGLAB](https://sccn.ucsd.edu/eeglab/index.php) program:

* Install EEGLAB
* Run EEGLAB in MATLAB
* File > Load existing dataset > 
* Select your received file (default 'result.set')
* Plot > Component maps > In 2-D >
* Set component numbers (for example, 1:16) > OK




