function y = g(x)

I = ones(size(x), 1);
y = I./(I+exp(-x));

end