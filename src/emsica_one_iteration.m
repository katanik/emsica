function new_B=emsica_one_iteration(nb_activities, tao, B, L, W, u, beta, iteration)

disp(['EMSICA> Iteration ', num2str(iteration), ' was started ...']);

I = eye(nb_activities, nb_activities);
delta_B = B*((B'*get_phi_b(W, B, beta)) - I - (get_phi_u(u)*u')*(1.0/tao));
alpha = golden_section(B, W, delta_B, L, u, tao, beta);
new_B = B + alpha*delta_B;

disp(['EMSICA> Iteration ', num2str(iteration), ' was finished.']);

end