function y=p(x)
    %2014 :
    sech_x = sech(x);
    y = exp(-x.*x).*(sech_x.*sech_x);
    
    %2006 :
    %y = g(x).*(eye(size(g), 1)-g(x));
end