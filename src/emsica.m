tao = 20;
nb_iterations = 1;
beta = 1.0;

[L, W, nb_sources, nb_cells] = get_leadfield_matrix();
[x, nb_times, nb_epochs, eeg_file] = get_eeg_data(nb_sources);
nb_activities = nb_sources;
B = get_initial_value(nb_activities, L);

%for a while
nb_epochs = 1;

for epoch = 1 : nb_epochs
    %count u
    A = L*B;
    u = zeros(nb_activities, tao);
    parfor t = 1 : tao
        u(:, t)=A\x(:, t, epoch);
    end
    
    %count new B
    for iteration = 1 : nb_iterations
        B = emsica_one_iteration(nb_activities, tao, B, L, W, u, beta, iteration);
    end
end

%ICA components are replaced with EMSICA components for the image
display_result(L*B, eeg_file);