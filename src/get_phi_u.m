function phi_u = get_phi_u(u)

%my formula (2014)
phi_u = -2.0 * (tanh(u)+u);

%my formula (2006)
%I=ones(nb_activities, tao);
%phi_u = (-1.0*I) - 2.0*(I./(I+exp(-1.0*u)));

%Tsai's formula (2006)
%I=ones(nb_activities, tao);
%phi_u = I - 2.0*(I./(I+exp(-1.0*u)));

end