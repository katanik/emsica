function display_result(A, eeg_file)

load(eeg_file, '-mat');
EEG.icaweights=inv(A);
EEG.icawinv=A;
[file, path] = uiputfile('*.set','Select file to save');
save([path, file], 'EEG');
msgbox({'File ', [path, file], ' was created.'}, 'Message', 'help');

end