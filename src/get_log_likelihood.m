function l=get_log_likelihood(B, W, L, u, tao, beta)

[nb_cells, nb_activities] = size(B);

sum_b = 0.0;
parfor k = 1 : nb_activities
    f = B(:, k)'*(W*(W'))*B(:, k);
    sech_Bk = sech(B(:,k));
    sum_b = sum_b + sum(log(exp(-beta*f)*(sech_Bk.*sech_Bk)));
end

sum_u = 0.0;
parfor t=1:tao
    sum_u=sum_u+sum(log(p(u(:, t))));
end

l = sum_b-log(det(L*B))+(1.0/tao)*sum_u;
end