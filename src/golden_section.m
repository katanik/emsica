function alpha=golden_section(B, W, delta_B, L, u, tao, beta)

disp('EMSICA> Search alpha using golden section method ... ');
eps=0.01;
max_iteration = 15;

fi=(1.0+sqrt(5.0))/2.0;
left=0.0;
right=100.0;
x1=right-(right-left)/fi;
x2=left+(right-left)/fi;
f_x1=get_log_likelihood(B+x1*delta_B, W, L, u, tao, beta);
f_x2=get_log_likelihood(B+x2*delta_B, W, L, u, tao, beta);

iteration=0;

while abs(right-left)>=eps && iteration<max_iteration
    disp(['EMSICA> The golden section method> Iteration ', num2str(iteration), ' ...']);
    iteration=iteration+1;
    if (f_x1>f_x2)
        left=x1;
        x1=x2;
        f_x1=f_x2;
        x2=right-(x1-left);
        f_x2=get_log_likelihood(B+x2*delta_B, W, L, u, tao, beta);
    else
        right=x2;
        x2=x1;
        f_x2=f_x1;
        x1=left+(right-x2);
        f_x1=get_log_likelihood(B+x1*delta_B, W, L, u, tao, beta);
    end
end
alpha=left;

%alpha=0.001;
disp(['EMSICA> Search alpha have finished: alpha = ', num2str(alpha), '.']);

end