function [x,nb_times,nb_epochs, eeg_file]=get_eeg_data(nb_sources)

[file_name, path, filter_index] = uigetfile({'*.set','SET-files (*.set)'}, ...
    'Select the file with EEG data');

eeg_file = [path, file_name];

if filter_index~=0
    load([path, file_name], '-mat');
    x=EEG.data;
    [colomns,nb_times,nb_epochs]=size(x);
    if nb_sources~=colomns
        msgbox('MRI does not match the EEG-data.', 'Error', 'Error');
    end
    disp('EMSICA> EEG data was loaded successfully.');
else
    msgbox('File with EEG data have not loaded.', 'Error', 'Error');
end

end