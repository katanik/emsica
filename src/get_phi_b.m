function phi_b = get_phi_b(W, B, beta)

[nb_cells, nb_activities] = size(B);
phi_b = zeros(nb_cells, nb_activities);
r=(W*W)';

for k = 1 : nb_activities
    parfor j = 1 : nb_cells
        %my formula:
        phi_b(j, k) = -2.0*tanh(B(j, k)) - beta*r(j, :)*B(:, k);
        %Tsai's formula:
        %phi_b(j, k)= -2.0 * (beta*r(j, :)*B(:, k) - tanh(B(j,k)));
    end
end

end