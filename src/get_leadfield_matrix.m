function [L, W, nb_sources, nb_cells]=get_leadfield_matrix()

[FileName,PathName,FilterIndex] = uigetfile({'*.mat','MAT-files (*.mat)'}, ...
    'Select the file with the leadfield matrix');
if FilterIndex~=0
    HeadModel=load([PathName,FileName]);
    if isfield(HeadModel,{'Gain', 'GridOrient'})
        L = bst_gain_orient(HeadModel.Gain, HeadModel.GridOrient);
        [nb_sources, nb_cells]=size(L);
        
        W=eye(nb_cells, nb_cells);
        for i=1:nb_cells
            for j=1:nb_sources
                W(i, i)=W(i,i)+L(j, i)*L(j, i);
            end
            W(i,i)=sqrt(W(i,i)-1);
        end
        disp('EMSICA> Leadfield matrix was loaded successfully.');
    else
        msgbox('Variable "Gain" or "GridOrient" are not found.', 'Error', 'Error');
        return
    end
else
    msgbox('File with leadfield matrix was not loaded.', 'Error', 'Error');
end
end