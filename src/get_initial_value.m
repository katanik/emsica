function B=get_initial_value(nb_activities, L)

[nb_sources, nb_cells] = size(L);

E=eye(nb_sources, nb_activities);
B=L\E;

% random from a to b:
%a=-1;
%b=1;
%B=(a + (b-a).*rand(nb_cells, nb_activities))*0.01;

%{
[FileName,PathName,FilterIndex] = uigetfile({'*.mat','MAT-files (*.mat)'}, ...
    'Select the file with the weigth matrix');
if FilterIndex~=0
    initial_data=load([PathName,FileName]); 
    if isfield(initial_data,'ImagingKernel') 
         B=initial_data.ImagingKernel;
         [rows, colomns]=size(B);
         if rows~=nb_cells || colomns~=nb_activities
            msgbox('Size of the B matrix is not correspond to the expected size.', 'Error', 'error');
         end
    else
        warndlg('Variable "ImagingKernel" not found.','Error');
        return
    end
   % msgbox('Weight matrix loaded successfully.', 'Error', 'help');
else
    msgbox('File with initial value of B matrix have not loaded.', 'Error', 'error');
end
%}

end